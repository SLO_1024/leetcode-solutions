Done:

JavaScript:  \
977. Squares of a Sorted Array:  \
https://leetcode.com/problems/squares-of-a-sorted-array/
  \
771. Jewels and Stones:  \
https://leetcode.com/problems/jewels-and-stones/
  \
709. To Lower Case:  \
https://leetcode.com/problems/to-lower-case/

---

Python3:  \
709. To Lower Case:  \
https://leetcode.com/problems/to-lower-case/

---

SQL:  \
175. Combine Two Tables:  \
https://leetcode.com/problems/combine-two-tables/
  \
181. Employees Earning More Than Their Managers:  \
https://leetcode.com/problems/employees-earning-more-than-their-managers/
  \
182. Duplicate Emails:  \
https://leetcode.com/problems/duplicate-emails/
  \
595. Big Countries:  \
https://leetcode.com/problems/big-countries/
  \
197. Rising Temperature:  \
https://leetcode.com/problems/rising-temperature/
  \
183. Customers Who Never Order:  \
https://leetcode.com/problems/customers-who-never-order/
  \
511. Game Play Analysis I: \
https://leetcode.com/problems/game-play-analysis-i/