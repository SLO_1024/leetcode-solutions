-- Question:
-- https://leetcode.com/problems/customers-who-never-order/

SELECT name AS Customers FROM Customers
LEFT JOIN Orders
ON Orders.customerId = Customers.id
WHERE Orders.customerId IS NULL