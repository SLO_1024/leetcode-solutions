-- Question:
-- https://leetcode.com/problems/rising-temperature/

SELECT new.id FROM Weather new, Weather old
WHERE new.temperature > old.temperature 
AND new.recordDate = DATE_ADD(old.recordDate, Interval 1 day)