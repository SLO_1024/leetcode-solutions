-- Question: 
-- https://leetcode.com/problems/employees-earning-more-than-their-managers/

SELECT a.name AS Employee FROM Employee AS a
INNER JOIN Employee AS b 
ON a.managerId=b.id 
WHERE a.salary > b.salary