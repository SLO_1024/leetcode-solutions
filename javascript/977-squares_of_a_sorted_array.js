// Question: 
// https://leetcode.com/problems/squares-of-a-sorted-array/

var sortedSquares = function(nums) {
    let numsArr = []
    for (let i = 0; i < nums.length; i++){
        numsArr.push(nums[i]**2)
    };
        numsArr.sort(function(first, second) {
                     return first - second
                     })
    return numsArr
};
