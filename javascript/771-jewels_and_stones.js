// Question: 
// https://leetcode.com/problems/jewels-and-stones/

var numJewelsInStones = function(J, S) {
    let num = 0;
    for(let s of S){
      for(let j of J){
          if (s === j){
              num ++
          }
      }
    }
    return num;
}